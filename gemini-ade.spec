%define _prefix /gem_base/epics/epics-base
%define gemopt opt
%define name gemini-ade
%define repository gemdev
%define debug_package %{nil}
%define arch %(uname -m)
%define checkout %(git log --pretty=format:'%h' -n 1) 

# These global defines are added to prevent stripping
# symbols on vxWorks cross-compiled code
# Getting 'strip' to work is probably only needed for
# building a related debug sub-package
#
# But this prevents all the strip warnings
# mrippa 20120202
%global _enable_debug_package 0
%global debug_package %{nil}
%global __os_install_post /usr/lib/rpm/brp-compress %{nil}

Summary: Gemini Application Development Environment: docker package
Name: %{name}
Version: 2.2
Release: 3
License: EPICS Open License
Group: Applications/Engineering
Source0: %{name}-%{version}.tar.gz
ExclusiveArch: %{arch}
Prefix: %{_prefix}
BuildRequires: net-tools 
Requires: net-tools rsync tito tdct perl-interpreter xauth rclone
Provides: /bin/perl


%description
The ADE2 is used at Gemini to provide a applocation development environment. It is splitted into two packages, one for the host providing the ADE commands
to manage the various docker-based environments and one for the development containers providing the work environment inside those. This package provides
the latter one.

%package host
Summary: Gemini Application Development Environment: host package
Conflicts: gemini-ade
Group: Applications/Engineering
BuildRequires: net-tools 
Requires: net-tools rsync tito perl-interpreter xauth

%description host
The ADE2 is used at Gemini to provide a applocation development environment. It is splitted into two packages, one for the host providing the ADE commands
to manage the various docker-based environments and one for the development containers providing the work environment inside those. This package provides
the host part.

%prep
%setup -q

%build

%install
rm -rf %{buildroot}

mkdir -p %{buildroot}/%{_prefix}

cp -r bin %{buildroot}/%{_prefix}/
cp -r etc  %{buildroot}/
cp -r gem_base %{buildroot}/


%post
# change group ownership and permissions for /gem_base to let users
# create directories for testing installations there
if grep -q local-users /etc/group
then 
	chgrp local-users /gem_base
	chmod 775 /gem_base
else
	if grep -q users /etc/group
	then
		chgrp users /gem_base
		chmod 775 /gem_base
	fi
fi

# set suid bit 
chmod u+s /usr/sbin/alternatives

# create dirs for gem-alternatives
mkdir -p /gem_base/etc/alternatives
mkdir -p /gem_base/lib/alternatives

/sbin/ldconfig


%postun
# unset suid bit 
chmod u-s /usr/sbin/alternatives

/sbin/ldconfig

%clean
## Usually you won't do much more here than
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root)
# uncomment for now, bwcause already provided by epics-base package. Should be moved here.
   /%{_prefix}/bin/linux-x86_64/convertGemRelease.pl
   /etc/profile.d/ade.sh
   /gem_base/etc
   /gem_base/usr
   /gem_base/bin/ade2-machines.sh
   /gem_base/bin/gem-init-project.sh
   /gem_base/bin/gem-ci-update.sh
   /gem_base/bin/gem-epics-config-update.sh
   /gem_base/bin/gem-svn2git-tags.py
   /gem_base/epics

%files host
%defattr(-,root,root)
# uncomment for now, bwcause already provided by epics-base package. Should be moved here.
   /etc/profile.d/ade.sh
   /gem_base/etc
   /gem_base/usr
   /gem_base/bin/ade2-machines.sh
   /gem_base/bin/gem-env*.sh
   /gem_base/bin/gem-exec*.sh
   /gem_base/bin/gem-init-project.sh
   /gem_base/bin/gem-ci-update.sh
   /gem_base/bin/gem-epics-config-update.sh
   /gem_base/bin/gem-svn2git-tags.py
   /gem_base/bin/gem-build-env*.sh
   /gem_base/bin/gem-repo*.sh
   /gem_base/bin/gem-init-ade.sh
   /gem_base/bin/gem-add-group-runner.sh


%changelog

