
# ADE2 rclone configuration directory
#export RCLONE_CONFIG_DIR="$HOME/.ade2/config/rclone"
export RCLONE_CONFIG_DIR="$HOME/.config/rclone"
export REPO_DRIVE_DIR="$HOME/.ade2/repo"
# ADE2 gitlab-runner configuration directory
export GL_RUNNER_CONFIG_DIR="$HOME/.ade2/config/gitlab-runner"
export GL_RUNNER_DATA_DIR="$HOME/.ade2/data/docker"
# gitlab configuration directory
export GL_CONFIG_DIR="$HOME/.ade2/config/gitlab"
# ADE2 secrets directory
export SECRETS_DIR="$HOME/.ade2/secrets"
# ADE2 Build Node docker-compose file path
export DC_CONFIG="$(dirname $0)/../etc/build-node/docker-compose.yml"
# ADE2 webserver docker-compose file path
export DC_WEBSERVER_CONFIG="$(dirname $0)/../etc/web-server-docker-compose.yml"

# determine wether in hbf or sbf networks
export SITE="default"

# Test first access to GS repo, as we can access hbfswgrepo-lv1 from both
# sites, defeating the point
if nc -z sbfswgrepo-lv1.cl.gemini.edu 22 &> /dev/null; then 
    export SITE="sbf"
elif nc -z hbfswgrepo-lv1.hi.gemini.edu 22 &> /dev/null; then 
    export SITE="hbf"
fi

# If newly generated, the referring private key has to be stored
# in base64 format as rtsw group variable SSH_PRIVATE on gitlab.com.
# base64 encoding can be achieved by the command
#   cat <SSH_PRIVATE_KEY_FILE> | base64 -w 0
export PUBLIC_KEY=$(cat "$(dirname $0)/../etc/build-node/ssh/id_rsa_buildnode.pub")

export ID=$(id -u) 
export GID=$(id -g) 
export USER_NAME=$(whoami | tr '~!@#$%^&*+=:;.,' '_')  

export COMPOSE_PROJECT_NAME="ade2"


function gem_env_init
{
if ! (which docker &> /dev/null) && ! (cat /.dockerenv &> /dev/null); then
    echo "docker is missing. Please install.
 You might follow these steps:
 https://docs.docker.com/engine/install/centos/#installation-methods
 
 Quick Steps:
 sudo yum install -y yum-utils
 sudo yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo
 sudo yum install docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin --allowerasing
 sudo systemctl start docker
 sudo systemctl enable docker
 sudo groupadd docker
 sudo usermod -aG docker $USER
 newgrp docker
 "
    exit 1
fi

if [ -d ".git"  ]; then
# determine layer
REMOTE=`git remote get-url origin`
REMOTE_PATH=${REMOTE%/*}
REMOTE_SUBGROUP=${REMOTE_PATH##*/}

if [ "$REMOTE_SUBGROUP" == "iocs" ]; then
	TYPE="app"
elif [ "$REMOTE_SUBGROUP" == "epics-base" ] ; then
	TYPE="epics-base"
elif [ "$REMOTE_SUBGROUP" == "support" ] ; then
	TYPE="epics-support"
elif [ "$REMOTE_SUBGROUP" == "common" ]; then
	TYPE="common"
else
	echo "No known type for software module (i.e. common, epics-base, epics-support or app), exiting ..."
    exit 1
fi
echo ""
echo "Type (layer name): $TYPE"
echo ""

# defaults
USERNAME=$(id -un)
BRANCHNAME=$(git rev-parse --abbrev-ref HEAD)
BRANCHNAME_SLUG=$(echo $BRANCHNAME | sed 's/\//-/g')
DOCKER_IMG_URL="registry.gitlab.com/nsf-noirlab/gemini/rtsw/gem-ci/${TYPE}_${BRANCHNAME}"
CONTAINER_PREFIX=${USER_NAME}_${BRANCHNAME_SLUG}
CONTAINER_PREFIX_AND_TYPE=${CONTAINER_PREFIX}-${TYPE}
CONTAINER_QUOTED=$(docker ps --format '{{json .Names}}' | grep $CONTAINER_PREFIX_AND_TYPE)
# remove quotes at beginning and end
temp="${CONTAINER_QUOTED%\"}"
CONTAINER="${temp#\"}"
else
echo "
Not within an git project, exiting ..."
exit 1
fi
}
