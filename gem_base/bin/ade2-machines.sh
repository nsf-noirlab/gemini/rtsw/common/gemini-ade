#!/bin/bash 

function show_machines
{

header="%-5s %-30s\t%s\n"
echo ====================ADE2 Gemini North Machines=================
printf "$header" "[1]" "mkortnfs-lv1.hi.gemini.edu"     "10.2.71.30" 	
printf "$header" "[2]" "mkorpmfs-lv1.hi.gemini.edu"     "10.2.71.32"
printf "$header" "[3]" "hbfswgrepo-lv1.hi.gemini.edu"   "10.1.71.31"
printf "$header" "[4]" "hbfrtnfs-lv1.hi.gemini.edu"     "10.1.71.30"
printf "$header" "[5]" "hbfrpmfs-lv1.hi.gemini.edu"     "10.1.71.32"
printf "$header" "[6]" "hbfswgade-lv1.hi.gemini.edu"    "10.1.71.13"


echo =====================ADE2 Gemini South Machines================
printf "$header" "[1]" "cportnfs-lv1.cl.gemini.edu"     "172.17.71.21" 
printf "$header" "[2]" "cporpmfs-lv1.cl.gemini.edu"     "172.17.71.22"
printf "$header" "[3]" "hbfswgrepo-lv1.cl.gemini.edu"   "10.1.71.31"
printf "$header" "[6]" "sbfswgade-lv1.cl.gemini.edu"    "172.17.71.17"      
echo
}

show_machines

