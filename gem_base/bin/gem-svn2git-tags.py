#!/usr/bin/env python3
'''
Script to convert tags from SVN to Git, after migrating an SVN repo.
This script should be run after executing "git svn clone" command
'''

import subprocess as sp
import argparse

def parse_args():
    '''
    This routines parses the arguments used when running this script
    '''
    parser = argparse.ArgumentParser(
        description='This script converts SVN tag branches to Git tags')

    parser.add_argument('-v', '--verbose',
                        action='store_true',
                        dest='verb',
                        default=False,
                        help='Toggle ouput verbosity (default = No verbosity)')

    parser.add_argument('-pc', '--partial-command',
                        dest='pc',
                        choices = ['full', 'tag', 'dbr'],
                        default='full',
                        help='Select partial command execution, "tag" for \
                        tag creation only; "dbr" for branch deletion only \
                        (Default = "full"')

    args = parser.parse_args()
    return args

if __name__ == '__main__':
    args = parse_args()
    # Define the command to get all refs information
    ref_cmd = "git for-each-ref --format='%(refname)' refs/remotes"
    # Execute the command. Catch exceptions if any and terminate the script
    # execution
    if args.verb:
        print('Executing: {}'.format(ref_cmd))
    try:
        git_refs_raw = sp.run([ref_cmd],
                            shell=True,
                            stdout=sp.PIPE,
                            stderr=sp.PIPE,
                            encoding='utf-8')
    except Exception as e:
        print(str(e))
        exit(0)

    # Check to see if the actual command ended with errors
    if not(git_refs_raw.stderr):
        # Gather all the refs associated with tags
        if args.verb:
            print('Refs found:')
            print(git_refs_raw.stdout)
        git_tag_refs = [gr.split('/') \
                        for gr in git_refs_raw.stdout.split('\n') \
                        if 'tags' in gr.split('/')[:-1]]

        if not(git_tag_refs):
            print('No tag branches found')
            exit(0)

        # Go through each tag ref to create a tag and delete corresponding
        # branch
        for ref in git_tag_refs:
            # Define the arguments for git tag and branch command
            svn_tag = '/'.join(ref[1:])
            git_ref = '/'.join(ref)
            tag_branch = '/'.join(ref[2:])
            git_tag = ref[-1]
            # git tag definition and execution
            # Execute only if full or tag option selected
            if args.pc in ['full','tag']:
                # git tag commit message
                cm_msg = '"Convert {0} to git tag"'.format(svn_tag)
                if args.verb:
                    print('Converting {0}:'.format(svn_tag))
                # git tag command definition
                tag_cmd = 'git tag -a {0} -m {1} {2}'.format(git_tag,
                                                            cm_msg,
                                                            git_ref)
                if args.verb:
                    print('Executing: {}'.format(tag_cmd))
                try:
                    create_tag = sp.run([tag_cmd],
                                    shell=True,
                                    stdout=sp.PIPE,
                                    stderr=sp.PIPE,
                                    encoding='utf-8')
                except Exception as e:
                    print(str(e))
                    exit(0)
                print(create_tag.stderr)
            # git branch Delete command definition and execution.
            # Execute only if full or dbr option selected
            if args.pc in ['full','dbr']:
                del_br_cmd = 'git branch -r -D {0}'.format(tag_branch)
                if args.verb:
                    print('Executing: {}'.format(del_br_cmd))
                try:
                    del_branch = sp.run([del_br_cmd],
                                    shell=True,
                                    stdout=sp.PIPE,
                                    stderr=sp.PIPE,
                                    encoding='utf-8')
                except Exception as e:
                    print(str(e))
                    exit(0)
                if args.verb:
                    print(del_branch.stdout)
                print(del_branch.stderr)

        print('Converting Done!')
    else:
        print(git_refs_raw.stderr)
