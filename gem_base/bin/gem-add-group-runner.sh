#!/bin/bash
shopt -s expand_aliases
alias trace_on='set -x'
alias trace_off='{ set +x; } 2>/dev/null'

if [ $# -ne 1 ]; then
echo please submit username for gitlab runner
exit 1
else
GL_USER=$1
fi

# ADE2 gitlab-runner configuration directory
export GL_RUNNER_CONFIG_DIR="/tmp/config/gitlab-runner"
export GL_RUNNER_DATA_DIR="/tmp/data/docker"

# check if a gitlab-runner with tag_list=ADE2-Build-Node-$USER is registered at
# gitlab.com and with the same id in the local gitlab-runner configuration
id=$(curl -s --header "PRIVATE-TOKEN: $(cat $HOME/.ade2/secrets/gl-api-access-token.txt 2> /dev/null)" \
       "https://gitlab.com/api/v4/runners?tag_list=ADE2-Build-Node-$USER" | jq '.[] | .id')
if ! grep -E "id = $id"  $GL_RUNNER_CONFIG_DIR/config.toml &> /dev/null; then
    GL_RUNNER_CONFIGURED=0
fi


if (( ! $GL_RUNNER_CONFIGURED )); then
    echo
    echo "
--- PLEASE READ CAREFULLY ---

The gitlab-runner configuration process is started now to create the 
configuration file for a local ADE2 build node for user $GL_USER 

During the process, eventually existing runner configurations on 
gitlab.com for runners with the same name will be deleted.

Press any key to continue
"
    read

    # obtain nsf-noirlab/gemini/rtsw group_id
    echo receiving gitlab.com rtsw group id...
    GL_RTSW_GROUP_ID=$(curl -s https://gitlab.com/api/v4/groups?search=nsf-noirlab%2Fgemini%2Frtsw \
      | jq -r '.[] | select(.name=="rtsw") | "\(.id)"')

    # obtain runners with tag_list=ADE2-Build-Node-$USER and delete them
    echo receiving gitlab.com runner list for runners with tag_list=ADE2-Build-Node-$GL_USER...
    runners=$(curl -s --header "PRIVATE-TOKEN: $(cat $HOME/.ade2/secrets/gl-api-access-token.txt)" "https://gitlab.com/api/v4/runners?tag_list=ADE2-Build-Node-$GL_USER" | jq '.[] | .id')
    echo
    echo deleting gitlab.com runners with tag_list=ADE2-Build-Node-$GL_USER
    for id in $runners; do 
      echo - deleting runner with id $id
      curl --request DELETE \
        --header "PRIVATE-TOKEN: $(cat $HOME/.ade2/secrets/gl-api-access-token.txt)" \
        "https://gitlab.com/api/v4/runners/$id"
    done
    echo

    # create a gitlab runner on gitlab.com
    echo creating runner on gitlab.com...
    GL_RUNNER_TOKEN=$(curl -sX POST https://gitlab.com/api/v4/user/runners \
      --data runner_type=group_type \
      --data "group_id=$GL_RTSW_GROUP_ID" \
      --data "description=ADE2 Build Node" \
      --data "tag_list=ADE2-Build-Node-$GL_USER" \
      --header "PRIVATE-TOKEN: $(cat $HOME/.ade2/secrets/gl-api-access-token.txt)" \
      | jq -r '.token')

    echo initializing local runner instance...
    mkdir -p $GL_RUNNER_CONFIG_DIR
    cp $(dirname $0)/../etc/build-node/config.toml.default $GL_RUNNER_CONFIG_DIR/config.toml
    mkdir -p $GL_RUNNER_DATA_DIR
    docker run --rm -it \
      -v $GL_RUNNER_CONFIG_DIR:/etc/gitlab-runner \
      gitlab/gitlab-runner:latest register \
      --non-interactive \
      --url https://gitlab.com \
      --token $GL_RUNNER_TOKEN \
      --name "ADE2 Build Node" \
      --executor docker \
      --docker-image "rockylinux:8" \
      --docker-volumes=/var/run/docker.sock:/var/run/docker.sock \
      --docker-network-mode gemini-ade-net 

    echo runner config written to $GL_RUNNER_CONFIG_DIR/config.toml. 
    echo Please submit to user \'$GL_USER\' for storing it under
    echo '$HOME/.ade/config/gitlab-runner/config.toml' 
    echo
    echo done. Press any key to continue
    read
 fi
