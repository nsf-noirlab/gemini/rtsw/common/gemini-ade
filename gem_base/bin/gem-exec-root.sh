#!/bin/bash
# using docker and therefore commenting out the below
##SERVICE=`docker compose ps --services`
##docker compose  exec  $SERVICE $@

. "$(dirname $0)/../etc/bash/gem-env.sh"
gem_env_init

echo "Please enter the container to be used
[ $CONTAINER ]:"
read CONTAINER_INPUT
if [ "$CONTAINER_INPUT" != "" ]; then
    CONTAINER=$CONTAINER_INPUT
fi

docker exec -it --env DISPLAY=$DISPLAY --env XAUTHORITY=$XAUTH -w $PWD $CONTAINER $@

