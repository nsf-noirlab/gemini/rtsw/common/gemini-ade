#!/bin/bash

source $(dirname $0)/gem-init-ade.sh -b

export COMPOSE_PROJECT_NAME="gemci"

echo -n "logging in to gitlab.com registry ... "
cat $SECRETS_DIR/gl-api-access-token.txt | docker login registry.gitlab.com -u $(cat $GL_CONFIG_DIR/gl_user.txt) --password-stdin

#docker compose -p "ade2-buildenv" -f ${DC_CONFIG} down --rmi all
#docker image prune
#docker compose -p "ade2-buildenv" -f ${DC_CONFIG} pull
#docker compose -p "ade2" -f ${DC_WEBSERVER_CONFIG} up -d --remove-orphans
#docker compose -p "ade2-buildenv" -f ${DC_CONFIG} up -d

docker compose -p "gemci" -f ${DC_CONFIG} down --rmi all
docker image prune
docker compose -p "gemci" -f ${DC_CONFIG} pull
docker compose -p "gemci" -f ${DC_CONFIG} up -d
