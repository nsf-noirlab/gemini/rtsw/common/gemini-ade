#!/bin/bash

source $(dirname $0)/../etc/bash/gem-env.sh

docker compose -p "gemci" -f ${DC_CONFIG} down --rmi all
docker image prune
