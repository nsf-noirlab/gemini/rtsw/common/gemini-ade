#!/bin/bash

source $(dirname $0)/gem-init-ade.sh

## parsing conmmandline options
for i in $*; do
    case $1 in
        -i|--image) IMAGE="$2"; shift 2;;
        -p|--service-prefix) PREFIX="$2"; shift 2;;
        -h|--help) usage; exit 1;;
        -*) usage; exit 1;;
    esac
done


if [ -d ".git"  ]; then

# If init was not yet done but we need a devel container and a container name default.
# I don't remember why I added this. I'll comment it out for now.
#if [ ! -f ".gitlab-ci.yml" ]; then
#    touch .gitlab-ci.yml
#fi

if [ "$IMAGE" != "" ]; then
DOCKER_IMAGE_URL=$IMAGE
else
echo "Please enter the container URL to use as development environment.
[ $DOCKER_IMG_URL ]:"
read DOCKER_IMG_URL_INPUT
if [ "$DOCKER_IMG_URL_INPUT" != "" ]; then
    DOCKER_IMG_URL=$DOCKER_IMG_URL_INPUT
fi
fi

if [ "$PREFIX" != "" ]; then
CONTAINER_PREFIX=$PREFIX
else
echo "Please enter the name prefix to use for development container.
[ $CONTAINER_PREFIX ]:"
read CONTAINER_PREFIX_INPUT
if [ "$CONTAINER_PREFIX_INPUT" != "" ]; then
    CONTAINER_PREFIX=$CONTAINER_PREFIX_INPUT
fi
fi

export REPO_DRIVE_DIR="$HOME/.ade2/repo"
export DC_WEBSERVER_CONFIG="$(dirname $0)/../etc/web-server-docker-compose.yml"
export DC_ENV_CONFIG="$(dirname $0)/../etc/$TYPE/docker-compose.yml"
export CONTAINERFILE="$(dirname $0)/../etc/Containerfile_XWin"

export IMAGE=$DOCKER_IMG_URL
export COMPOSE_PROJECT_NAME=$CONTAINER_PREFIX 
export BUILD_CONTEXT="$(dirname $0)/../etc"

export ID=$(id -u)

# use deprecated builder for now, because name resolution does not work with 
# default buildx builder
export DOCKER_BUILDKIT=0

echo "Pulling newest docker image and creating container from it..."
# COMPOSE_PROJECT_NAME env variable evaluation is built-in to docker compose
docker compose -f ${DC_ENV_CONFIG} down --rmi all
docker image rm -f "${IMAGE}"
docker image rm -f "${IMAGE}-patched"
docker image prune -f -a
docker compose -f ${DC_ENV_CONFIG} pull 
# start web server hosting the RPM repo through rclone mounted volume
# manually build using docker command, name resolution does not work with buildx and 
# the deprecated builder makes problems with docker compose build
#docker image rm -f ${IMAGE#*/}
docker compose -p "ade2" -f ${DC_WEBSERVER_CONFIG} up -d 
docker build -t "${IMAGE}-patched" "$BUILD_CONTEXT" --no-cache  -f ${CONTAINERFILE} \
 --build-arg IMAGE=$IMAGE --build-arg REPOSET=${SITE} --network=gemini-ade-net
export IMAGE="${IMAGE}-patched"
docker compose -f ${DC_ENV_CONFIG} up -d

else
echo "
Not within an git project, exiting ..."
exit 1
fi
