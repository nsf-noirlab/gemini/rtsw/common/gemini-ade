#!/bin/bash


XAUTH=${HOME}/.docker.xauth
echo > $XAUTH
xauth nlist $DISPLAY | sed -e 's/^..../ffff/' | xauth -f $XAUTH nmerge -

# if in ssh remote session
if [ -n "$SSH_CLIENT" ] || [ -n "$SSH_TTY" ]; then
  SESSION_TYPE="ssh"
fi
if [ "$SESSION_TYPE" == "ssh" ]; then
  trap 'kill $(jobs -p)' EXIT
  HOST_DOCKER_GATEWAY=$(/sbin/ip route|awk '/docker0/ { print $9 }')

  X11PORT=`echo $DISPLAY | sed 's/^[^:]*:\([^\.]\+\).*/\1/'`
  TCPPORT=`expr 6000 + $X11PORT`
  socat TCP-LISTEN:$TCPPORT,fork,bind=$HOST_DOCKER_GATEWAY TCP:127.0.0.1:$TCPPORT &

  DISPLAY=`echo $DISPLAY | sed 's/^[^:]*\(.*\)/'"$HOST_DOCKER_GATEWAY"'\1/'`
fi


. "$(dirname $0)/../etc/bash/gem-env.sh"
gem_env_init

echo "Please enter the container to be used
[ $CONTAINER ]:"
read CONTAINER_INPUT
if [ "$CONTAINER_INPUT" != "" ]; then
    CONTAINER=$CONTAINER_INPUT
fi

docker exec -it --user=$(id -u):$(id -g) --env USER=$USER --env DISPLAY=$DISPLAY --env XAUTHORITY=$XAUTH -w $PWD $CONTAINER $@

