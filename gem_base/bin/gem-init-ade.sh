#!/bin/bash

shopt -s expand_aliases
alias trace_on='set -x'
alias trace_off='{ set +x; } 2>/dev/null'

source $(dirname $0)/../etc/bash/gem-env.sh
gem_env_init

# force overwriting
OPT_FORCE=0
# create ADE2 Build Node
OPT_BUILDNODE=0

# 1 when rclone is configured. Assume it to be so and default it to 1
RCLONE_CONFIGURED=1
# 1 when a gitlab API access token is existing. Assume it to be so and default it to 1
GL_API_ACCESS_CONFIGURED=1
# 1 when gitlab-runner is configured. Assume it to be so and default it to 1
GL_RUNNER_CONFIGURED=1


# usage
function usage
{
echo "USAGE:
`basename $0` [-f|--force] 
    [-b|--buildnode]
	[-h|--help]

This script 
 - initialises rclone to be able to mount a shared drive which 
   contains the shared RPM repositories provided by ADE2.
 - guides through the generation of an user-specific gitlab-CI 
   API access token  and stores it in the user's home directory.
 - creates a local gitlab-runner container infrastructure 

Afterwards, the rclone configuration can be used by the containerized 
ADE2 environment.

 -f|--force     forces overwriting of existing configurations
 -b|--buildnode adds configurations steps to create a ADE2 build node
                wrapped into a containerized gitlab-runnder environment
"
}

#trace_on
# check if rclone is installed
if ! command -v rclone &> /dev/null; then
    echo rclone command not found
    exit 1
fi

# check if fuse is configured correctly for rclone
if ! grep -Eq '^user_allow_other' /etc/fuse.conf; then
echo "Please edit /etc/fuse.conf to set 'user_allow_other', e.g.:
$ cat /etc/fuse.conf
# mount_max = 1000
user_allow_other
"
exit 1
fi
# check if rclone is configured. A token has to be present in the configuration
if ! grep -E '^token' $RCLONE_CONFIG_DIR/rclone.conf &>/dev/null; then
    RCLONE_CONFIGURED=0
fi

# check if gitlab API access token is existing...
if ! grep -E '^glpat-' $SECRETS_DIR/gl-api-access-token.txt &>/dev/null; then
    GL_API_ACCESS_CONFIGURED=0
fi

# ... and valid
echo retreiving data from gitlab.com
RETVAL=$(curl -s https://gitlab.com/api/v4/groups?search=nsf-noirlab%2Fgemini%2Frtsw \
  --header "PRIVATE-TOKEN: $(cat $HOME/.ade2/secrets/gl-api-access-token.txt 2> /dev/null)" \
  | jq -r 'keys[0]')
if [ "$RETVAL" == "error" ]; then
    GL_API_ACCESS_CONFIGURED=0
fi

# check if a gitlab runner named "ADE2" has already been configured in the
# local gitlab-runner configuration
#if ! grep -E -i 'name = "ADE2' $GL_RUNNER_CONFIG_DIR/config.toml &>/dev/null; then
#    GL_RUNNER_CONFIGURED=0
#fi

# check if a gitlab-runner with tag_list=ADE2-Build-Node-$USER is registered at
# gitlab.com and with the same id in the local gitlab-runner configuration
if [ "$(hostname)" == "hbfswgade-lv1.hi.gemini.edu" ]; then
    id=$(curl -s --header "PRIVATE-TOKEN: $(cat $HOME/.ade2/secrets/gl-api-access-token.txt 2> /dev/null)" \
       "https://gitlab.com/api/v4/runners?tag_list=ADE2-Build-Node-hbfade" | jq '.[] | .id')
else
    id=$(curl -s --header "PRIVATE-TOKEN: $(cat $HOME/.ade2/secrets/gl-api-access-token.txt 2> /dev/null)" \
       "https://gitlab.com/api/v4/runners?tag_list=ADE2-Build-Node-$USER" | jq '.[] | .id')
fi
if ! grep -E "id = $id"  $GL_RUNNER_CONFIG_DIR/config.toml &> /dev/null || [ "$id" == "" ]; then
    GL_RUNNER_CONFIGURED=0
fi

## parsing conmmandline options
for i in $*; do
    case $1 in
        -f|--force) OPT_FORCE=1; shift;;
        -b|--buildnode) OPT_BUILDNODE=1; shift;;
        -h|--help) usage; exit 1;;
        -*) usage; exit 1;;
    esac
done

if (( ! $RCLONE_CONFIGURED )) || (( $OPT_FORCE )); then
    echo
    echo "
--- PLEASE READ CAREFULLY ---

The rclone or sshfs (on hbfade) configuration process is started now. 

This needs authentication with the Gemini shared RTU team drive. You 
need to have read/write access to this drive. 

Please follow the instructions and use the default values except when
working on a headless machine, like stated in the configuration dialog
below.

Press any key to continue
"
    read

    echo creating $RCLONE_CONFIG_DIR
    mkdir -p $RCLONE_CONFIG_DIR
    echo creating $REPO_DRIVE_DIR
    mkdir -p $REPO_DRIVE_DIR
#    echo copying default ADE2 docker environment settings to $ENV_FILE
#    cp $(dirname $0)/../etc/build-node/env.default $ENV_FILE
    echo "copying default rclone configuration file to $RCLONE_CONFIG_DIR/rclone.conf

    "
    cp $(dirname $0)/../etc/build-node/rclone.conf.default $RCLONE_CONFIG_DIR/rclone.conf

    # create config from template and force user
    # to generate token, but only if not on hbfade
    # where we sshfs-mount the repo
    if [ "$(hostname)" != "hbfswgade-lv1.hi.gemini.edu" ] && \
       [ "$(hostname)" != "sbfswgade-lv1.cl.gemini.edu" ]; then
        rclone config reconnect repo:
    fi

    ## docker based rclone does not work with sssd users
    # for now
#    if [ -e "/sys/fs/cgroup" ]; then
#    docker run  -it --rm \
#        --volume $RCLONE_CONFIG_DIR:/config/rclone \
#        --volume $REPO_DRIVE_DIR:/data:shared \
#        --user $(id -u):$(id -g) \
#        --volume /etc/passwd:/etc/passwd:ro \
#        --volume /etc/group:/etc/group:ro \
#        --volume /etc/nsswitch.conf:/etc/nsswitch.conf:ro \
#        --volume /sys/fs/cgroup:/sys/fs/cgroup:ro \
#        --volume /var/lib/sss:/var/lib/sss:rw \
#        --device /dev/fuse \
#        --cap-add SYS_ADMIN \
#        --security-opt apparmor:unconfined  \
#        --network "host" \
#        rclone/rclone  config reconnect repo: 
#    else
#    docker run  -it --rm \
#        --volume $RCLONE_CONFIG_DIR:/config/rclone \
#        --volume $REPO_DRIVE_DIR:/data:shared \
#        --user $(id -u):$(id -g) \
#        --volume /etc/passwd:/etc/passwd:ro \
#        --volume /etc/group:/etc/group:ro \
#        --device /dev/fuse \
#        --cap-add SYS_ADMIN \
#        --security-opt apparmor:unconfined  \
#        --network "host" \
#        rclone/rclone  config reconnect repo: 
#    fi
    echo done. Press any key to continue
    read
else
    echo "rclone configured."
fi

echo -n "checking repo mount "
if grep -qs "$REPO_DRIVE_DIR" /proc/mounts; then
    echo ... found.
else
    echo ... not found, mounting $REPO_DRIVE_DIR.
    if [ "$SITE" == "hbf" ]; then
        trace_on
        sshfs koji@hbfswgrepo-lv1:/rtsw/packages $HOME/.ade2/repo -o ssh_command="ssh -o IdentitiesOnly=yes" -o allow_other
        trace_off
    elif [ "$SITE" == "sbf" ]; then
        trace_on
        sshfs koji@sbfswgrepo-lv1:/rtsw/packages $HOME/.ade2/repo -o ssh_command="ssh -o IdentitiesOnly=yes" -o allow_other
        trace_off
    else
        rclone mount repo: $REPO_DRIVE_DIR --allow-non-empty --allow-other --vfs-cache-mode full --umask 0 --daemon
    fi
#    if [ -e "/sys/fs/cgroup" ]; then
#    docker run  -d \
#        --name rclone \
#        --volume $RCLONE_CONFIG_DIR:/config/rclone \
#        --volume $REPO_DRIVE_DIR:/data:shared \
#        --user $(id -u):$(id -g) \
#        --volume /etc/passwd:/etc/passwd:ro \
#        --volume /etc/group:/etc/group:ro \
#        --volume /etc/nsswitch.conf:/etc/nsswitch.conf:ro \
#        --volume /sys/fs/cgroup:/sys/fs/cgroup:ro \
#        --volume /var/lib/sss:/var/lib/sss:rw \
#        --device /dev/fuse \
#        --cap-add SYS_ADMIN \
#        --security-opt apparmor:unconfined  \
#        --network "host" \
#        rclone/rclone mount repo: /data --allow-non-empty --allow-other
#    else
#    docker run  -d \
#        --name rclone \
#        --volume $RCLONE_CONFIG_DIR:/config/rclone \
#        --volume $REPO_DRIVE_DIR:/data:shared \
#        --user $(id -u):$(id -g) \
#        --volume /etc/passwd:/etc/passwd:ro \
#        --volume /etc/group:/etc/group:ro \
#        --device /dev/fuse \
#        --cap-add SYS_ADMIN \
#        --security-opt apparmor:unconfined  \
#        --network "host" \
#        rclone/rclone mount repo: /data --allow-non-empty --allow-other
#    fi
fi

if (( ! $GL_API_ACCESS_CONFIGURED )) || (( $OPT_FORCE )); then
    echo
    echo "
--- PLEASE READ CAREFULLY ---

The gitlab.com API access token retrieval process will start now. 

This needs authentication with gitlab.com and access to the group
'nsf-noirlab/gemini/rtsw' on API level.

Please visit  
    https://gitlab.com/-/user_settings/personal_access_tokens

- Press [Add new token]
- Enter a name 
- Delete the 'Expiration date' field with [x] button to extend validity period
- Check the [] api checkbox 
- Press 'Create personal access token'. 

Please enter the retrieved API access token (copy and Ctrl-Shift-V):
"
    read API_ACCESS_TOKEN
echo "
Please enter your gitlab user name:
"
    read GL_USER
    echo creating $SECRETS_DIR
    mkdir -p $SECRETS_DIR
    echo storing token in $SECRETS_DIR/gl-api-access-token.txt
    echo $API_ACCESS_TOKEN > $SECRETS_DIR/gl-api-access-token.txt
    echo storing gitlab username in $GL_CONFIG_DIR
    mkdir -p $GL_CONFIG_DIR
    echo $GL_USER > $GL_CONFIG_DIR/gl_user.txt
    echo done. Press any key to continue
    read
else
    echo "gitlab API access token present and valid."
fi

echo -n "logging in to gitlab.com registry ... "
cat $SECRETS_DIR/gl-api-access-token.txt | docker login registry.gitlab.com -u $(cat $GL_CONFIG_DIR/gl_user.txt) --password-stdin


if (( ! $GL_RUNNER_CONFIGURED )) && (( $OPT_BUILDNODE )) \
     || (( $OPT_FORCE )) && (( $OPT_BUILDNODE )); then
    echo
    echo "
--- PLEASE READ CAREFULLY ---

The gitlab-runner configuration process is started now to create a 
local ADE2 build node. 

During the process, eventually existing runner configurations on 
gitlab.com for runners with the same name will be deleted.

Press any key to continue
"
    read

    # obtain nsf-noirlab/gemini/rtsw group_id
    echo receiving gitlab.com rtsw group id...
    GL_RTSW_GROUP_ID=$(curl -s https://gitlab.com/api/v4/groups?search=nsf-noirlab%2Fgemini%2Frtsw \
      | jq -r '.[] | select(.name=="rtsw") | "\(.id)"')

    if [ "$(hostname)" == "hbfswgade-lv1.hi.gemini.edu" ]; then
        # obtain runners with tag_list=ADE2-Build-Node-hbfade and delete them
        echo receiving gitlab.com runner list for runners with tag_list=ADE2-Build-Node-hbfade...
        runners=$(curl -s --header "PRIVATE-TOKEN: $(cat $HOME/.ade2/secrets/gl-api-access-token.txt)" "https://gitlab.com/api/v4/runners?tag_list=ADE2-Build-Node-hbfade" | jq '.[] | .id')
        echo
        echo deleting gitlab.com runners with tag_list=ADE2-Build-Node-hbfade
    else
        # obtain runners with tag_list=ADE2-Build-Node-$USER and delete them
        echo receiving gitlab.com runner list for runners with tag_list=ADE2-Build-Node-$USER...
        runners=$(curl -s --header "PRIVATE-TOKEN: $(cat $HOME/.ade2/secrets/gl-api-access-token.txt)" "https://gitlab.com/api/v4/runners?tag_list=ADE2-Build-Node-$USER" | jq '.[] | .id')
        echo
        echo deleting gitlab.com runners with tag_list=ADE2-Build-Node-$USER
    fi
    for id in $runners; do 
      echo - deleting runner with id $id
      curl --request DELETE \
        --header "PRIVATE-TOKEN: $(cat $HOME/.ade2/secrets/gl-api-access-token.txt)" \
        "https://gitlab.com/api/v4/runners/$id"
    done
    echo

    # create a gitlab runner on gitlab.com
    echo creating runner on gitlab.com...
    if [ "$(hostname)" == "hbfswgade-lv1.hi.gemini.edu" ]; then
        GL_RUNNER_TOKEN=$(curl -sX POST https://gitlab.com/api/v4/user/runners \
          --data runner_type=group_type \
          --data "group_id=$GL_RTSW_GROUP_ID" \
          --data "description=ADE2 Build Node" \
          --data "tag_list=ADE2-Build-Node-hbfade" \
          --header "PRIVATE-TOKEN: $(cat $HOME/.ade2/secrets/gl-api-access-token.txt)" \
          | jq -r '.token')
    else
        GL_RUNNER_TOKEN=$(curl -sX POST https://gitlab.com/api/v4/user/runners \
          --data runner_type=group_type \
          --data "group_id=$GL_RTSW_GROUP_ID" \
          --data "description=ADE2 Build Node" \
          --data "tag_list=ADE2-Build-Node-$USER" \
          --header "PRIVATE-TOKEN: $(cat $HOME/.ade2/secrets/gl-api-access-token.txt)" \
          | jq -r '.token')
    fi

    echo initializing local runner instance...
    mkdir -p $GL_RUNNER_CONFIG_DIR
    cp $(dirname $0)/../etc/build-node/config.toml.default $GL_RUNNER_CONFIG_DIR/config.toml
    mkdir -p $GL_RUNNER_DATA_DIR
    docker run --rm -it \
      -v $GL_RUNNER_CONFIG_DIR:/etc/gitlab-runner \
      -v $GL_RUNNER_DATA_DIR:/var/lib/docker \
      gitlab/gitlab-runner:alpine register \
      --non-interactive \
      --url https://gitlab.com \
      --token $GL_RUNNER_TOKEN \
      --name "ADE2 Build Node" \
      --executor docker \
      --docker-image "rockylinux:8" \
      --docker-volumes=/var/run/docker.sock:/var/run/docker.sock \
      --docker-volumes=/etc/pki/entitlement:/etc/pki/entitlement \
      --docker-volumes=/etc/rhsm:/etc/rhsm \
      --docker-network-mode gemci_buildenv-net 

    echo done. Press any key to continue
    read
elif (( $OPT_BUILDNODE )); then
    echo runner configuration present.
fi

if (( ! $OPT_BUILDNODE )); then
echo -n "checking for docker bridge network gemini-ade-net "
if docker network inspect gemini-ade-net &> /dev/null ; then 
    echo ... found
else 
    echo ... not found, creating it
    docker network create gemini-ade-net
    echo restarting web server container to provide access to shared cloud storage
    export DC_WEBSERVER_CONFIG="$(dirname $0)/../etc/web-server-docker-compose.yml"
    export COMPOSE_PROJECT_NAME="ade2"
    docker compose -f ${DC_WEBSERVER_CONFIG} down
    docker compose -f ${DC_WEBSERVER_CONFIG} pull
    docker compose -f ${DC_WEBSERVER_CONFIG} up -d --remove-orphans
fi
fi
