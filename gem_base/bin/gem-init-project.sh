#!/bin/bash
shopt -s expand_aliases
alias trace_on='set -x'
alias trace_off='{ set +x; } 2>/dev/null'

# force overwriting
OPT_FORCE=0
# create dev env, i.e. container
OPT_DEVENV=0
# sync upstream
OPT_SYNC=0
# continuous deployment to yum repos
OPT_CD=0
# container layer to deploy for
TYPE=""
# branch of gem-ci submodule to use
GEM_CI_BRANCH=master

# usage
function usage
{
echo "USAGE:
`basename $0` [-f|--force] 
    [-e|--dev-env]
    [-b|--gem-ci-branch]
	[-h|--help]

This script initialises a project directory to contain a 
proper configuration to start developing and releasing
to the Gemini testing and stable RPM repositories.

 -f|--force forces overwriting of existing configuration files
 -e|--dev-env interactively creates development environment by
        pulling referring container and checking framework settings
 -b|--gem-ci-branch specifies branch of gem-ci to be used.
        Defaults to master.
"
}

# initialize tito environment
function init_tito
{
if [ ! -d ".tito" ] || (( $OPT_FORCE )); then
if [ -d ".git"  ]; then
echo "
copying ADE's tito template files"
trace_on
cp -r "$(dirname $0)/../usr/share/tito/skeleton/.tito" .
trace_off
echo "
initializing tito and symlinking (potentially not YET existing) 
/gem_base/usr/share/tito/$TYPE/releasers.conf to .tito/"
trace_on
# remove .tito/releasers.conf from .gitignore
echo "checking .gitignore .. "
grep -q -e '/.tito/releasers.conf' .gitignore && (
sed -i  '/^\/\.tito\/releasers\.conf/d' .gitignore &&
echo ' deleted line:
    /.tito/releasers.conf'
)
tito init
# releasers.conf will be symlinked in the next step for local tests,
# but for container creation it is generated
git rm -f .tito/releasers.conf || echo ".tito/releasers.conf not found, perfect, continueing"
ln -snf /gem_base/usr/share/tito/$TYPE/releasers.conf .tito/releasers.conf
git add .tito/*
trace_off
else
echo "
Not within an git project, exiting ..."
exit 1
fi
fi
}

# initialize CD environment
function init_cd
{
# are we in a git project directory?
if [ -d ".git"  ]; then

# --force option set
if [ ! -f ".gitlab-ci.yml" ] || (( $OPT_FORCE )); then
echo "
initializing .gitlab-ci.yml for continuous deployment"
trace_on

# If init was not yet done but we need a devel container and a container name default.
if [ ! -f ".gitlab-ci.yml" ]; then
    touch .gitlab-ci.yml
fi

for module in gem-ci gem-rtsw-repo
do
    if [ -d $module ]; then
        # Remove the submodule entry from .git/config
        git submodule deinit -f $module 
        # Remove the submodule directory from the superproject's .git/modules directory
        rm -rf .git/modules/$module
        # Remove the entry in .gitmodules and remove the submodule directory located at path/to/submodule
        git rm -f --cached $module
	rm -rf $module
    fi
done

git submodule add -b $GEM_CI_BRANCH ../../gem-ci
git submodule update --init --recursive gem-ci
BASE_CONTAINER_CURRENT=$(cat .gitlab-ci.yml | grep BASE_CONTAINER: | cut -d'"' -f2)
sed -e 's#ref:.*$#ref: "'"$GEM_CI_BRANCH"'"#' \
    -e 's#<BRANCH>#'"$BRANCHNAME"'#' \
    gem-ci/templates/gitlab-ci/$TYPE.gitlab-ci.yml > .gitlab-ci.yml
BASE_CONTAINER_AUTO=$(cat .gitlab-ci.yml | grep BASE_CONTAINER: | cut -d'"' -f2)


trace_off

# handle BASE_CONTAINER setting
echo "
Please enter URL to be used as base container for this project, 
[Enter] to keep the current one, or [a] to use a autogenerated URL.

Be aware, that the BASE_CONTAINER needs to be at least the same 
maturity level as the current project, so a testing-level project 
must not have a unstable-level BASE_CONTAINER.
The BASE_CONTAINER variable is ignored, if there already is a project 
on the same layer that has the same branch name.

Now enter the new URL or press
[enter] for current URL  [ $BASE_CONTAINER_CURRENT ]
or [a] for auto-created [ $BASE_CONTAINER_AUTO ]:"
read BASE_CONTAINER_INPUT
if [ "$BASE_CONTAINER_INPUT" == "a" ]; then
    BASE_CONTAINER=$BASE_CONTAINER_AUTO
elif [ "$BASE_CONTAINER_INPUT" == "" ]; then
    BASE_CONTAINER=$BASE_CONTAINER_CURRENT
else
    BASE_CONTAINER=$BASE_CONTAINER_INPUT
fi


sed -i 's#BASE_CONTAINER:.*$#BASE_CONTAINER: "'"$BASE_CONTAINER"'"#' .gitlab-ci.yml

git add gem-ci .gitlab-ci.yml 
trace_off
else
echo "
No -f option set, omitting to overwrite .gitlab-ci.yml ..."
fi

echo "

Now project-specific git-options will be set for configuring
which CI pipeline to use.

The options are to use a set of options
- to connect to the default (legacy) deployment environment configured on 
  Gemini-internal resources (hbfswgade-lv1, ...)
- to connect to a developer-specific deployment environment on the developer's 
  machine provided by a docker(-compose)-orchestrated set of containers in an
  automated manner
- to be configured individually one by one: 
    * the gitlab-runner tag list
    * the rsync URL of the RPM repository
    * the username for accessing it
    * the ssh URL and port of the machine hosting the RPM repository
      (can usually be directly derived from the rsync URL)

Please choose an option:
"

PS3="Your choice (1|2|3): "
select opt in "hbfade" "developer-specific" "individual"
do
case $opt in
  "hbfade" )
    git config --unset-all push.pushOption ci.variable
    git config --add push.pushOption ci.variable="GEMCI_TAGS=ADE2-Build-Node-hbfade"
    git config --add push.pushOption ci.variable="GEMCI_RSYNC_BASEURL=ade2-rpmrepo-openssh-server:/repo"
    git config --add push.pushOption ci.variable="GEMCI_RPMREPO_HOSTNAME=http://ade2-rpmrepo-web-server"
    git config --add push.pushOption ci.variable="GEMCI_RPMREPO_BASEURL=http://ade2-rpmrepo-web-server"
    git config --add push.pushOption ci.variable="GEMCI_RSYNC_USERNAME=ade2user"
    git config --add push.pushOption ci.variable="GEMCI_RSYNC_PORT=2222"
    ;;
  "developer-specific" )
    git config --unset-all push.pushOption ci.variable
    git config --add push.pushOption ci.variable="GEMCI_TAGS=ADE2-Build-Node-$USER"
    git config --add push.pushOption ci.variable="GEMCI_RSYNC_BASEURL=ade2-rpmrepo-openssh-server:/repo"
    git config --add push.pushOption ci.variable="GEMCI_RPMREPO_HOSTNAME=http://ade2-rpmrepo-web-server"
    git config --add push.pushOption ci.variable="GEMCI_RPMREPO_BASEURL=http://ade2-rpmrepo-web-server"
    git config --add push.pushOption ci.variable="GEMCI_RSYNC_USERNAME=ade2user"
    git config --add push.pushOption ci.variable="GEMCI_RSYNC_PORT=2222"
    ;;
  "individual")
    git config --unset-all push.pushOption ci.variable
    echo enter GEMCI_TAGS:
    read GEMCI_TAGS
    git config --add push.pushOption ci.variable="GEMCI_TAGS=$GEMCI_TAGS"
    echo enter GEMCI_RSYNC_BASEURL:
    read GEMCI_RSYNC_BASEURL
    git config --add push.pushOption ci.variable="GEMCI_RSYNC_BASEURL=$GEMCI_RSYNC_BASEURL"
    echo enter GEMCI_RPMREPO_HOSTNAME:
    read GEMCI_RPMREPO_HOSTNAME
    git config --add push.pushOption ci.variable="GEMCI_RPMREPO_HOSTNAME=$GEMCI_RPMREPO_HOSTNAME"
    echo enter GEMCI_RPMREPO_BASEURL:
    read GEMCI_RPMREPO_BASEURL
    git config --add push.pushOption ci.variable="GEMCI_RPMREPO_BASEURL=$GEMCI_RPMREPO_BASEURL"
    echo enter GEMCI_RSYNC_USERNAME:
    read GEMCI_RSYNC_USERNAME
    git config --add push.pushOption ci.variable="GEMCI_RSYNC_USERNAME=$GEMCI_RSYNC_USERNAME"
    echo enter GEMCI_RSYNC_PORT:
    read GEMCI_RSYNC_PORT
    git config --add push.pushOption ci.variable="GEMCI_RSYNC_PORT=$GEMCI_RSYNC_PORT"
    ;;
  *) echo invalid option $REPLY
esac
echo "
git push options set:"
git config --get-all push.pushOption ci.variable || echo none
# only one time choice, otherwise select loops
break
done

else
echo "
Not within an git project, exiting ..."
exit 1
fi

}


# initialize upstream syncing environment
function init_sync
{
if [ ! -f ".gitlab-ci.yml" ] || (( $OPT_FORCE )); then
if [ -d ".git"  ]; then
echo "
initializing .gitlab-ci.yml for upstream syncing and continuous deployment"
trace_on
cp -f "$(dirname $0)/../usr/share/gitlab/ci/upstream/.gitlab-ci.yml"
trace_off
else
echo "
Not within an git project, exiting ..."
exit 1
fi
fi
}

#initialize development environment i.e. container
function init_devenv
{
if (( $OPT_DEVENV )); then
$(dirname $0)/gem-env-up.sh
fi
}

# initialize project
function init_project
{
. "$(dirname $0)/../etc/bash/gem-env.sh"
gem_env_init
init_cd
init_devenv
init_tito
}


## parsing conmmandline options
for i in $*; do
    case $1 in
        -f|--force) OPT_FORCE=1; shift;;
#    	-u|--integrate-upstream) OPT_SYNC=1; shift;;
#	    -d|--continuous-deployment) OPT_CD=1; shift;;
#        -l|--layer) TYPE="$2"; shift 2;;
        -e|--dev-env) OPT_DEVENV=1; shift;;
        -b|--gem-ci-branch) GEM_CI_BRANCH="$2"; shift 2;;
        -h|--help) usage; exit 1;;
        -*) usage; exit 1;;
    esac
done

init_project
$(dirname $0)/gem-epics-config-update.sh
