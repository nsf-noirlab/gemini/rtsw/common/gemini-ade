#!/bin/sh
echo "
Executing script $0"

# Only do all of the below if we are within an EPICS project.
# Use confirue/RELEASE as marker for that
if [[ -f configure/RELEASE && "$PROJECTNAME" != "epics-base" ]]; then

echo "checking configure/RELEASE .. "
grep -q -e '-include $(TOP)/../RELEASE.local' configure/RELEASE || ( 
echo '
# include local RELEASE configuration file (this will be ignored by with
# .gitignore to keep it local and prevent poisoning the default build 
# scheme)
# overwrite configuration from above with this file and do not edit
# this one
#
# These lines allow developers to override these RELEASE settings
# without having to modify this file directly.
-include $(TOP)/../RELEASE.local' >> configure/RELEASE &&
echo ' added: 
    -include $(TOP)/../RELEASE.local'
)


grep -q -e '-include $(TOP)/../RELEASE.$(EPICS_HOST_ARCH).local' configure/RELEASE || (
echo '-include $(TOP)/../RELEASE.$(EPICS_HOST_ARCH).local' >> configure/RELEASE &&
echo ' added: 
    -include $(TOP)/../RELEASE.$(EPICS_HOST_ARCH).local'
)


grep -q -e '-include $(TOP)/configure/RELEASE.local' configure/RELEASE || (
echo '-include $(TOP)/configure/RELEASE.local' >> configure/RELEASE &&
echo ' added: 
    -include $(TOP)/configure/RELEASE.local'
)


# add includes to configure/CONFIG_SITE, if not existing
echo "checking configure/CONFIG_SITE .. "
grep -q -e '-include $(TOP)/../CONFIG_SITE.local' configure/CONFIG_SITE || (
echo '
# These allow developers to override the CONFIG_SITE variable
# settings without having to modify the configure/CONFIG_SITE
# file itself.
-include $(TOP)/../CONFIG_SITE.local' >> configure/CONFIG_SITE &&
echo ' added:
    -include $(TOP)/../CONFIG_SITE.local'
)

grep -q -e '-include $(TOP)/../configure/CONFIG_SITE.local' configure/CONFIG_SITE || (
echo '-include $(TOP)/../configure/CONFIG_SITE.local' >> configure/CONFIG_SITE &&
echo ' added:
    -include $(TOP)/../configure/CONFIG_SITE.local'
)

grep -q -e '-include $(TOP)/configure/CONFIG_SITE.local' configure/CONFIG_SITE || (
echo '-include $(TOP)/configure/CONFIG_SITE.local' >> configure/CONFIG_SITE &&
echo ' added:
    -include $(TOP)/configure/CONFIG_SITE.local'
)

# modify tdct.cfg to contain $(TESTING_INSTALL_DIR)/$(PROJECTNAME)/dbd in [dbdpath]
# use awk and not sed to only insert line after first occurence of pattern
tdctconfigfile=`find . -name tdct.cfg`
echo "checking $tdctconfigfile .. "
if [ ! -z "$tdctconfigfile" ] 
then
grep -q -e '$(TESTING_INSTALL_LOCATION)/$(PROJECTNAME)/dbd' $tdctconfigfile || (
awk -i inplace '{print} /^\[dbdpath\].*/ && !n  {print "[dbdpath] $(TESTING_INSTALL_LOCATION)/$(PROJECTNAME)/dbd/"; n++}' $tdctconfigfile &&
echo ' added $(TESTING_INSTALL_LOCATION)/$(PROJECTNAME)/dbd/ to [dbdpath]'
)
else 
echo "didn't find tdct.cfg"
fi

# add local config files to .gitignore
echo "checking .gitignore .. "
grep -q -e '/configure/RELEASE.local' .gitignore || (
echo '/configure/RELEASE.local' >> .gitignore &&
echo ' added:
    /configure/RELEASE.local'
)

grep -q -e '/configure/CONFIG_SITE.local' .gitignore || (
echo '/configure/CONFIG_SITE.local' >> .gitignore &&
echo ' added:
    /configure/CONFIG_SITE.local'
)

# create or patch configure/CONFIG_SITE.local
echo "checking configure/CONFIG_SITE.local .. "
project_name=`basename $PWD`
install_location=$TESTING_INSTALL_LOCATION/$project_name

if [ -f configure/CONFIG_SITE.local ]
then
grep -q -e "INSTALL_LOCATION = $install_location" configure/CONFIG_SITE.local || (
echo "INSTALL_LOCATION = $install_location" >> configure/CONFIG_SITE.local &&
echo " added:
    INSTALL_LOCATION = $install_location"
)
else 
echo "INSTALL_LOCATION = $install_location" >> configure/CONFIG_SITE.local &&
echo " created configure/CONFIG_SITE.local and added:
    INSTALL_LOCATION = $install_location"
fi

else
echo "
=====================================================================================
PLEASE NOTE that this script is for patching EPICS module, extension or IOC
sources configuration files. It won't touch other sources.
=====================================================================================

"
fi # [ -f configure/RELEASE ]
