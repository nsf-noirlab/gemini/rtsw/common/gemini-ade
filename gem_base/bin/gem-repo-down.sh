#!/bin/bash

# usage
function usage
{
echo "USAGE:
`basename $0`

This script stops a running deployment environment providing access
to RPMs of a specific release by deactivating the referring 
RPM repo file, stopping the webserver docker container and unmounting
the rclone-mounted directory providing the RPM repository accesss 
abstraction layer.

"
}

## parsing conmmandline options
if [ $# -gt 0 ]; then
    usage
    exit 1
fi

source $(dirname $0)/../etc/bash/gem-env.sh

echo stopping rclone $REPO_DRIVE_DIR
docker rm -f rclone
umount $REPO_DRIVE_DIR

echo shut down web server container
docker compose -p "ade2" -f ${DC_WEBSERVER_CONFIG} down --rmi all
docker image prune

echo remove gemini-ade-net
docker network rm gemini-ade-net &> /dev/null


echo for deactivating the repo file, 
echo  please enter your password to gain sudo access if prompted
sudo mv /etc/yum.repos.d/gem-rtsw.repo /etc/yum.repos.d/gem-rtsw.repo.deactivated
