#!/bin/bash

# usage
function usage
{
echo "USAGE:
`basename $0` <release> [<layer>]

This script initialises a deployment environment tp provide access
to RPMs of the specified release by installing a referring 
RPM repo file and a docker orchestrated webserver and cloud storage
abstraction layer (rclone).

Installing the repo file for the app/ioc layer is the default since
this is the only way it _should_ be used. However, the layer can also 
be specified by the second parameter

Examples:
* install the repo file for the app layer of 'stable/2024q1' release:
    `basename $0` stable/2024q1
* install the repo file for the common layer of 'stable/2024q1' release:
    `basename $0` stable/2024q1 common
"
}

# check for docker existence
if ! (which docker &> /dev/null) && ! (cat /.dockerenv &> /dev/null); then
    echo "docker is missing. Please install.
 You might follow these steps:
 https://docs.docker.com/engine/install/centos/#installation-methods
 
 Quick Steps:
 sudo yum install -y yum-utils
 sudo yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo
 sudo yum install docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin --allowerasing
 sudo systemctl start docker
 sudo systemctl enable docker
 sudo groupadd docker
 sudo usermod -aG docker $USER
 newgrp docker
 "
    exit 1
fi

## parsing conmmandline options
if [ $# -lt 1 ] || [ $# -gt 2 ]; then
    usage
    exit 1
fi

for i in $*; do
    case $1 in
        -h|--help) usage; exit 1;;
        -*) usage; exit 1;;
    esac
done

source $(dirname $0)/gem-init-ade.sh

# default 
TYPE="app"

if [ $# -eq 2 ]; then
TYPE="$2"
fi

echo checking layer: $TYPE

# Release in URL format (/ replaced with %2F}
RELEASE=${1/\//\%2F}


export REPO_DRIVE_DIR="$HOME/.ade2/repo"
export DC_WEBSERVER_CONFIG="$(dirname $0)/../etc/web-server-docker-compose.yml"
export SECRETS_DIR="$HOME/.ade2/secrets"

echo starting containerized webserver
docker compose -p "ade2" -f ${DC_WEBSERVER_CONFIG} down 
docker compose -p "ade2" -f ${DC_WEBSERVER_CONFIG} up -d --remove-orphans

echo retrieving repofile
REPOFILE_URL=$(curl -s --header "PRIVATE-TOKEN: $(cat $SECRETS_DIR/gl-api-access-token.txt)" "https://gitlab.com/api/v4/projects/nsf-noirlab%2Fgemini%2Frtsw%2Fgem-ci/environments?name=${TYPE}%2F${1/\/\%2F}" | jq -r '.[].external_url')

if [ "$REPOFILE_URL" == "" ]; then
    echo error retrieving repo URL
    exit 1
else
    echo retrieved repo file URL: $REPOFILE_URL
fi

# strip off http:// from the beginning of the URL
REPOFILE_URL_PATCHED_L1=${REPOFILE_URL#http://}
# get cuurent domain name 
REPOFILE_URL_DN=${REPOFILE_URL_PATCHED_L1%%/*}
# ... and strip it off
REPOFILE_URL_PATCHED_L2=${REPOFILE_URL_PATCHED_L1#$REPOFILE_URL_DN/}
REPOFILE_URL_PATCHED="http://localhost:7513/$REPOFILE_URL_PATCHED_L2"
REPO_URL_PATCHED=${REPOFILE_URL_PATCHED%/*}/
REPOFILE_TMP="/tmp/gem-rtsw.repo.$$"

echo patched repo file URL to: $REPOFILE_URL_PATCHED
echo and generated repo URL from it: $REPO_URL_PATCHED

# get repofile and try a few times for waiting for the webserver
# to come up
for i in {1..9}; do
    sleep 2
    wget $REPOFILE_URL_PATCHED -O $REPOFILE_TMP 
    test -s "$REPOFILE_TMP" && break
    # if not breaking: bad
    echo error downloading repofile
done
if ! test -s "$REPOFILE_TMP"; then
    echo exit therefore
    exit 1
fi

echo created repofile:

sed -e "s#baseurl=.*#baseurl=$REPO_URL_PATCHED#g" $REPOFILE_TMP > $REPOFILE_TMP.patched
cat $REPOFILE_TMP.patched

echo "Please enter user password to get root privilegies for
installing repo file"
sudo mv $REPOFILE_TMP.patched /etc/yum.repos.d/gem-rtsw.repo
sudo rm $REPOFILE_TMP

echo updating dnf cache
sudo dnf makecache

echo 
echo done
