#!/bin/bash

. "$(dirname $0)/../etc/bash/gem-env.sh"

if [ -d ".git"  ]; then
gem_env_init

## parsing conmmandline options
for i in $*; do
    case $1 in
        -p|--service-prefix) PREFIX="$2"; shift 2;;
        -h|--help) usage; exit 1;;
        -*) usage; exit 1;;
    esac
done

if [ "$PREFIX" != "" ]; then
CONTAINER_PREFIX=$PREFIX
else
echo "Please enter the container to be stopped and removed
[ $CONTAINER ]:"
read CONTAINER_INPUT
if [ "$CONTAINER_INPUT" != "" ]; then
    CONTAINER=$CONTAINER_INPUT
fi
fi

docker rm -f $CONTAINER
docker image prune -f -a
# docker system prune -f -a --volumes
fi
